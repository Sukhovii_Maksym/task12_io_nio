package controller;

import java.io.File;
import java.util.Scanner;

public class DirectoryController {
    public void printDirectory(File file) {
        System.out.println("| " + file.getName() + ": ");
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                printDirectory(f);
            } else {
                System.out.println("    " + f.getName());
            }
        }
    }

    public void activateCommands(File file) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Now you can use commands (type 'exit' to exit)");
        String input;
        while (!(input = sc.nextLine()).equalsIgnoreCase("exit")) {
            if (input.equalsIgnoreCase("cd")) {
                FileController fc = new FileController();
                printDirectory(fc.chooseDirectory());
            } else {
                if (input.substring(0, 2).equalsIgnoreCase("cd")) {
                    try {
                        String path = file.getAbsolutePath() + "\\" + input.substring(3).trim() + "\\";
                        System.out.println(path);
                        file = new File(path);
                        printDirectory(file);
                        activateCommands(file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (input.matches("dir")) {
                printDirectory(file);
            }
        }
    }
}
